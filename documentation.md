# FarmBot Genesis v1.5 documentation TL:DR

As it is open source, I will just link the documentation on their website.  

# Documentation from FarmBot

[Intro to FarmBot Genesis](https://genesis.farm.bot/v1.5/FarmBot-Genesis-V1.5/intro)  

[High Level Overview](https://genesis.farm.bot/v1.5/FarmBot-Genesis-V1.5/intro/high-level-overview.html)  

[Pre-Assembly instructions](https://genesis.farm.bot/v1.5/manufacturing/pre-assembly)
[Assembly preparation](https://genesis.farm.bot/v1.5/FarmBot-Genesis-V1.5/intro/assembly-preparation)  

[Supporting Infrastructure](https://genesis.farm.bot/v1.5/FarmBot-Genesis-V1.5/supporting-infrastructure)  
Note: The small genesis robot we have at UCL is available to be placed on mobile raised bed.

# Specifications

Version: farmbot genesis version 1.5  

Genesis model size:  
| Model  | Gantry Width  | Track Length  |  Area | Max Plant Height  |
|---|---|---|---|---|
|  Genesis	 | 1.5m  | 3m  | 4.5m2  | 0.5m  |  


# Sketch for mobile table

![Sketch on paper](Images/sketch_paper.jpg)

## Genesis Image

![Genesis Image](Images/farmbot_genesis_v1.5.png)

## Raised vs low tracks

![Raised vs low tracks](Images/raised_vs_low_tracks.png)
