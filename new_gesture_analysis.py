import cv2
import numpy as np
import math
from farmbot import Farmbot, FarmbotToken
import threading
import time

raw_token = FarmbotToken.download_token("gyti0045@edu.ucl.dk",
                                        "uclproject",
                                        "https://my.farm.bot")

# This token is then passed to the Farmbot constructor:
fb = Farmbot(raw_token)
print("Credentials converted to a token for safe transport.")
print("Authorisation token passed to Farmbot.")

class MyHandler:
    def on_connect(self, bot, mqtt_client):

        request_id1 = bot.move_absolute(x=10, y=20, z=30)
        # => "c580-6c-11-94-130002"
        print("MOVE_ABS REQUEST ID: " + request_id1)

        request_id2 = bot.send_message("Gesture control script started.")
        # => "2000-31-49-11-c6085c"
        print("SEND_MESSAGE REQUEST ID: " + request_id2)

    def on_change(self, bot, state):
        print("NEW BOT STATE TREE AVAILABLE:")
        print(state)

        print("Current position: (%.2f, %.2f, %.2f)" % bot.position())
        # A less convenient method would be to access the state
        # tree directly:
        pos = state["location_data"]["position"]
        xyz = (pos["x"], pos["y"], pos["z"])
        print("Same information as before: " + str(xyz))

    def on_log(self, bot, log):
        print("New message from FarmBot: " + log['message'])

    def on_response(self, bot, response):
        print("ID of successful request: " + response.id)

    def on_error(self, bot, response):
        print("ID of failed request: " + response.id)
        # We can also retrieve a list of error message(s) by
        # calling response.errors:
        print("Reason(s) for failure: " + str(response.errors))


print("Create a handler instance from the handler class.")
handler = MyHandler()
print("Connecting the handler to the robot.")


class ThreadingExample(object):
    def __init__(self, interval=1):
        """ Constructor
        :type interval: int
        :param interval: Check interval, in seconds
        """
        self.interval = interval

        thread = threading.Thread(target=self.run, args=())
        thread.daemon = True                            # Daemonize thread
        thread.start()                                  # Start the execution

    def run(self):
        """ Method that runs forever """
        while True:
            # Do something in the background thread
            print('Connecting to the robot in the background thread.')
            fb.connect(handler)
            print("This line will not execute. `connect()` is a blocking call.")

            time.sleep(self.interval)


# run the background thread
run_mqtt = ThreadingExample()
print("Starting the background thread function")

# the video analysis starts here, by designating a variable for the video capture
cap = cv2.VideoCapture(0)

# getting the w/h of camera capture resolution for testing camera specification quality
print("Testing camera resolution.")
width = int(cap.get(3))
height = int(cap.get(4))
print("Camera width and height:", width, height)
print("Launching the video analysis windows.")
print("To exit the script press 'q' button on the keyboard.")
print("Note: analysis freezes and continues when there is something in the ROI - region of interest(red rectangle).")

# main analysis loop while video capture is open
while True:

    try:  # script wrapped in try/except because it gives an error if nothing is in the ROI, crashing the script

        # read the camera capture
        ret, img = cap.read()
        # create and then define a rectangular ROI - region of interest
        cv2.rectangle(img, (400, 400), (100, 100), (0, 0, 255), 2)
        roi = img[100:400, 100:400]

        # converting the cropped img to grayscale, grayscale is needed for the thresholding
        gray = cv2.cvtColor(roi, cv2.COLOR_BGR2GRAY)

        # dilate the analysed video to fill dark spots within (depending on lighting in the area)
        #kernel = np.ones((3, 3), np.uint8)
        #gray = cv2.dilate(gray, kernel, iterations=4)

        # applying gaussian blur to the gray img (reduces sharpness and noise in video capture to analyse better)
        value = (15, 15)
        blurred = cv2.GaussianBlur(gray, value, 50)

        # thresholding the blurred img (inverted) - Otsu's Binarization method
        _, thresh = cv2.threshold(blurred, 120, 255,
                                  cv2.THRESH_BINARY_INV)

        # finding the contours and applying SIMPLE approximation method
        contours, hierarchy = cv2.findContours(thresh.copy(), cv2.RETR_TREE,
                                               cv2.CHAIN_APPROX_SIMPLE)

        # finding contour with max area (the gesture)
        cont = max(contours, key=lambda x: cv2.contourArea(x))

        # approximate the contour a little bit
        epsilon = 0.0005 * cv2.arcLength(cont, True)
        approx = cv2.approxPolyDP(cont, epsilon, True)

        # make a convex hull around the contour (the hand)
        hull = cv2.convexHull(cont)

        # define area of hull and area of hand
        areahull = cv2.contourArea(hull)
        areacont = cv2.contourArea(cont)

        # find the percentage of area not covered by hand in convex hull
        arearatio = ((areahull - areacont) / areacont) * 100

        # find the defects in convex hull with respect to hand
        hull = cv2.convexHull(approx, returnPoints=False)
        defects = cv2.convexityDefects(approx, hull)

        # l = no. of defects
        l = 0

        # finding no. of defects (the gesture that is shown)
        for i in range(defects.shape[0]):
            s, e, f, d = defects[i, 0]
            start = tuple(approx[s][0])
            end = tuple(approx[e][0])
            far = tuple(approx[f][0])
            pt = (100, 180)

            # finding the length of triangle sides between defects (finger points being the start and end, circle being far)
            a = math.sqrt((end[0] - start[0]) ** 2 + (end[1] - start[1]) ** 2)
            b = math.sqrt((far[0] - start[0]) ** 2 + (far[1] - start[1]) ** 2)
            c = math.sqrt((end[0] - far[0]) ** 2 + (end[1] - far[1]) ** 2)
            s = (a + b + c) / 2
            ar = math.sqrt(s * (s - a) * (s - b) * (s - c))

            # finding distance between point and convex hull
            d = (2 * ar) / a

            # applying cosine rule here
            angle = math.acos((b ** 2 + c ** 2 - a ** 2) / (2 * b * c)) * 57

            # ignore angles <= 90 and points near the convex hull, draw a circle on far point of the triangle between gesture
            if angle <= 90 and d > 30:
                l += 1
                cv2.circle(roi, far, 3, [0, 255, 0], -1)

            # draw lines around the hand
            cv2.line(roi, start, end, [255, 0, 0], 2)

        l += 1

        # finger detection
        ### parameters
        color = (255, 0, 0)
        font = cv2.FONT_HERSHEY_SIMPLEX
        org = (0, 50)
        ###
        if l == 1:
            if areacont < 2000:
                cv2.putText(img, "Show gesture", org, font, 2, color, 1)
            else:
                if arearatio < 12:
                    cv2.putText(img, "Detecting nothing", org, font, 2, color, 1)

                else:
                    cv2.putText(img, "Detecting 1 finger", org, font, 2, color, 1)

                    msg = 'Detecting gesture control script in action'
                    bot.send_message(msg, type="info")

        elif l == 2:
            cv2.putText(img, "Detecting 2 fingers", org, font, 2, color, 1)

            bot.take_photo()
            print("Taking photo on the robots' internal camera.")

        elif l == 3:
            cv2.putText(img, "Detecting 3 fingers", org, font, 2, color, 1)

            bot.read_status()

        elif l == 4:
            cv2.putText(img, "Detecting 4 fingers", org, font, 2, color, 1)

            bot.find_home()
            print("Bot trying to find the home position and return to it.")

        else:
            cv2.putText(img, "Detecting hand", org, font, 2, color, 1)

            bot.power_off()

    # capture and analysis windows
        cv2.imshow('Video Capture Window (img)', img)
        cv2.imshow('ROI grayscale', gray)
        cv2.imshow('ROI blurred', blurred)
        cv2.imshow('Otsu Threshold', thresh)

    except: # script wrapped in try/except because it gives an error if nothing is in the ROI, crashing the script
        pass

# exit the control script if "q" button is pressed on keyboard
    if cv2.waitKey(1) == ord('q'):
        print('Exiting via user request.')
        break
# release the camera capture and remove opened windows
cap.release()
cv2.destroyAllWindows()
