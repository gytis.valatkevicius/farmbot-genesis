# Information

We are IT Technology students in UCL, as of now we are interns on a project, and we ask for some help!  
***
We need FabLab assistance for manufacturing the mobile raised bed table with working area for UCL
FarmBot Genesis v1.5 robot. I will include what we have currently and what requirements we had
from LKLM (Automation Lab UCL SEEBLADSGADE).  
***
We need help with the table, designing it from the sketch I attach and the requirements list, we can help with deciding on the design regarding different choices of dimensions or other requirements, if it makes it more comfortable on the wooden ucl plates we have available.

# Requirements for mobile table

1. table with wheels for holding the FarmBot robot [mobile]  
	1.1 logos of UCL, FabLab, FarmBot Genesis V1.5 on the side [marking]  
2. has to be medium height for viewing and accessing with plants in mind [about 1 - 1.2 m maybe]   
3. use full structure board for better integrity  
4. Flat plane area for the robot testing with mountings is 1.5m x 3m [robot work area]  
	4.1 table itself needs to have a small work area separate to the robots working area, for laptop, tools, etc. [marked in sketch 1.5m x 0.5m]
	4.2 dimensions for the work space and drawer can be fitted to be comfortable to design, but should have space for laptop at the very least and drawer for tools, connectors, manual, etc  
5. Soil area with cover for the robot, the cover would act as testing plane/cover for dirt when not used or being moved.
	5.1 possibly water/dirt erosion protection would be needed for the wood plates available at UCL for furniture  

# Dimensions

Version: farmbot genesis version 1.5  

Genesis model size:  
| Model  | Gantry Width  | Track Length  |  Area | Max Plant Height  |
|---|---|---|---|---|
|  Genesis	 | 1.5m  | 3m  | 4.5m2  | 0.5m  |  

# Sketch for mobile table

![Sketch on paper](Images/sketch_paper.jpg)
