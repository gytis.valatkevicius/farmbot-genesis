# FarmBot Genesis

Setting up and modifying the genesis v1.5 farmbot robot at UCL.

***
![High-level diagram](Images/High-level diagram.png)
***

Genesis model size:  
| Model  | Gantry Width  | Track Length  |  Area | Max Plant Height  |
|---|---|---|---|---|
|  Genesis	 | 1.5m  | 3m  | 4.5m2  | 0.5m  |  

## Genesis Image

![Genesis Image](Images/farmbot_genesis_v1.5.png)
